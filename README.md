## Inslata o Nextcloud em container Docker

`git clone https://gitlab.com/Corolario/Nextclou-Docker.git`

Cria o certificados SSL auto-assinado

`cd sites-enabled && mkdir certbot && cd certbot &&
openssl req -x509 -nodes -days 90 -newkey rsa:2048 -subj "/C=BR/ST=SP/L=SaoPaulo/O=localhost/OU=TI/CN=localhost" -keyout privkey.pem -out cert.pem && cd ../..`

`docker-compose up -d`

`docker exec --user www-data some-nextcloudssl php occ db:convert-filecache-bigint`

docker run -it --rm -p 80:80 -v "$(pwd)/letsencrypt:/etc/letsencrypt" certbot/certbot certonly --standalone --staging --agree-tos --no-eff-email --email email@dom.com -d dominio.com -d www.dominio.com